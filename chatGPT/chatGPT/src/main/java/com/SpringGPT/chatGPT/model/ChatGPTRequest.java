package com.SpringGPT.chatGPT.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class ChatGPTRequest {
    private String model="gpt-3.5-turbo";
    private String prompt;
    private String temperature;
    @SerializedName(value = "max-token")
    private int maxToken=100;
}
