package com.SpringGPT.chatGPT.model;

import lombok.Data;

@Data
public class ChatGPTChoice {
    private String text;
}
