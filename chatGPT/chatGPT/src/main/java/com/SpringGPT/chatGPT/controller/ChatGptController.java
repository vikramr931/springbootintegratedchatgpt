package com.SpringGPT.chatGPT.controller;

import com.SpringGPT.chatGPT.model.SearchRequest;
import com.SpringGPT.chatGPT.service.ChatGPTService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ChatGptController {

    private ChatGPTService chatGPTService;

    public ChatGptController(ChatGPTService chatGPTService){
        this.chatGPTService=chatGPTService;
    }

    @PostMapping("/search")
    public String searchChatGpt(@RequestBody SearchRequest searchRequest){
        System.out.println("searchChatGPT Query :" + searchRequest.getQuery());
        return chatGPTService.processSearch(searchRequest.getQuery());
//        return "";
    }
}
